<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactController@dashboard');

Route::get('contacts/ajax', 'ContactController@ajax');
Route::get('contacts/getLatLng', 'ContactController@getLatLng');
Route::resource('contacts', 'ContactController');

