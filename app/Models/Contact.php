<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name',
        'email',
        'telephone'
    ];

    public function getTelephoneMaskedAttribute() {
        $value = $this->telephone;
        if (strlen($value) < 10) {
            return "--";
        }
        $ddd = substr($value, 0, 2);
        $final = substr($value, -4);
        return '('.$ddd.') '.str_replace([$ddd, $final], '', $value).'-'.$final;
    }

    public function setTelephoneAttribute($value) {
        $this->attributes['telephone'] = str_replace([' ', '(', ')', '-'], '', $value);
    }

    protected $table = 'contacts';

    /**
     * @return array[] Validation rules
     */
    public $rules =  [
            'name' => 'nullable|string',
            'email' => 'required|email|unique:contacts,email',
            'telephone' => 'numeric|nullable',
    ];

    public function address()
    {
        return $this->hasMany(Address::class);
    }
}
