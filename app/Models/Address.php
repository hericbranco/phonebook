<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'contact_id',
        'code',
        'uf',
        'city',
        'district',
        'street',
        'number',
        'lat',
        'lng'
    ];

    public function setCodeAttribute($value)
    {
        $this->attributes['code'] = str_replace([' ', '(', ')', '-'], '', $value);
    }

    protected $table = 'address';

    /**
     * @return array[] Validation rules
     */
    public $rules =  [
        'contact_id' => 'required|exists:contacts,id',
        'code' => 'required|numeric',
        'uf' => 'string|nullable',
        'city' => 'string|nullable',
        'district' => 'string|nullable',
        'street' => 'string|nullable',
        'number' => 'numeric|nullable',
        'lat' => 'string|nullable',
        'lng' => 'string|nullable'
    ];
}
