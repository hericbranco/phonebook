<?php
namespace App\Repositories;

use Log;
use App\Models\Contact;
use App\Models\Address;
use Validator;
use DB;

class ContactRepository
{
    public function save($arrParams=[])
    {
        try {
            DB::beginTransaction();
            extract($arrParams);
            $contact = new Contact();
            if (isset($id)) {
                $contact = Contact::find($id);
                if ($contact->email == $email) {
                    $contact->rules['email'] = 'required|email';
                }
            }
            if (isset($email) && $email != '') {
                $contact->email=$email;
            }
            if (isset($name) && $name != '') {
                $contact->name=$name;
            }
            if (isset($telephone) && $telephone != '') {
                $contact->telephone=$telephone;
            }
            $validator = Validator::make($contact->toArray(), $contact->rules);
            if ($validator->fails()) {
                throw new \Exception($validator->errors());
            }
            $contact->save();
            if (isset($arrParams['address']) && !empty($arrParams['address'])) {
                $this->_associateAddress($arrParams['address'], $contact->id);
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {            
            throw new \Exception($e->getMessage());
            return false;
        }
        
    }

    public function list()
    {
        try {
            $contacts = new Contact;
            return $contacts;
        } catch (\Exception $e) {
            return false;
        }
    }

    protected function _associateAddress($arrAddress=[], $contact_id)
    {
        try {
            $address = Address::firstOrNew(['contact_id' => $contact_id]);
            $address->fill($arrAddress);
            $validator = Validator::make($address->toArray(), $address->rules);
            if ($validator->fails()) {
                throw new \Exception($validator->errors());
            }
            return $address->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
            return false;
        }
    }

    public function delete(Contact $contact)
    {
        foreach (Address::where('contact_id',  $contact->id)->get() as $address) {
            $address->delete();
        }
        $contact->delete();
        return true;
    }
}
