<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Repositories\ContactRepository;
use Validator;
use DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repository = new ContactRepository;
        return view('contact.index', ['contacts' => $repository->list()->get()]);
    }

    public function dashboard()
    {
        return view('contact.dashboard');
    }

    public function ajax()
    {
        $repository = new ContactRepository;
        return $repository->list()->select([
            DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as date'),
            DB::raw('count(*) as total')
        ])
        ->groupBy(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d")'))
        ->get()
        ->pluck('total', 'date')
        ->toJson();
    }

    public function getLatLng()
    {
        $repository = new ContactRepository;
        $arrLatLng = [];
        foreach ($repository->list()->get() as $contact) {
            $arrLatLng[] = $contact->toArray()+$contact->address[0]->toArray();
            
        }
        return json_encode($arrLatLng);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('contact.form', [
            'name'=>'', 
            'email'=>'', 
            'telephone'=>'',
            'address' => [[
                'code' => '',
                'uf' => '',
                'city' => '',
                'district' => '',
                'street' => '',
                'number' => '',
                'lat' => '',
                'lng' => '',
            ]]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $arrAddress = Request(['code', 'uf', 'city', 'district', 'street', 'number', 'lat', 'lng']);
            $repository = new ContactRepository;
            $repository->save(Request(['name', 'email', 'telephone'])+['address' => $arrAddress]);
            return redirect('contacts')->with('success', 'save success');
        } catch(\Exception $e) {            
            return back()->withErrors(json_decode($e->getMessage()))->withInput(Request(['name', 'email', 'telephone'])+['address' => $arrAddress]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        return view('contact.form', $contact->toArray()+['address' => $contact->address]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        try {
            $arrAddress = Request(['code', 'uf', 'city', 'district', 'street', 'number', 'lat', 'lng']);
            $repository = new ContactRepository;
            $repository->save(Request(['name', 'email', 'telephone'])+['id' => $contact->id]+['address' => $arrAddress]);
            return redirect('contacts')->with('success', 'save success');
        } catch(\Exception $e) {            
            return back()->withErrors(json_decode($e->getMessage()))->withInput(Request(['name', 'email', 'telephone']));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $repository = new ContactRepository;
        $repository->delete($contact);        
        return redirect('contacts')->with('success','Product has been  deleted');
    }
}
