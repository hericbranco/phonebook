<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $n = rand(100, 300);        
        factory(\App\Models\Contact::class, $n)->create()->each(function ($contact) {
            $contact->address()->save(factory(App\Models\Address::class)->make());
            $contact->created_at = date('Y-m-d', strtotime($contact->created_at.'-'.rand(1, 35).' days'));
            $contact->save();
        });
        $this->command->info('contacts table seeded = ' . $n);
    }
}
