<?php

namespace Tests\Models;

use App\Models\Contact;
use Tests\TestCase;

class ContactTest extends TestCase
{
    public function testSave()
    {
        $contact = new Contact();
        $contact->email = "hericbranco@gmail.com";
        
        $this->assertTrue($contact->save());
    }
}
