<?php

namespace Tests\Models;

use App\Models\Contact;
use App\Models\Address;
use Tests\TestCase;

class AddressTest extends TestCase
{
    public function testSave()
    {
        $contact = new Contact();
        $contact->email = "hericbranco@gmail.com";
        $contact->save();
        $address = new Address();
        $address->fill([
            'contact_id' => $contact->id,
            'code' => '13030-380'
        ]);
        
        $this->assertTrue($address->save());
    }
}
