<?php
namespace Tests\Repositories;

use Tests\TestCase;
use App\Repositories\ContactRepository;
use App\Models\Contact;
use App\Models\Address;

class ContactRepositoryTest extends TestCase
{
    public function testSave()
    {
        $contactRepository = new ContactRepository;
        $this->assertTrue($contactRepository->save(['email' => 'heric@gmail.com']));
    }

    /**
     * @expectedException Exception
     */
    public function testSameEmailError() {
        $contactRepository = new ContactRepository;
        $contactRepository->save(['email' => 'heric@gmail.com']);
        $contactRepository->save(['email' => 'heric@gmail.com']);
    }

    public function testSameEmailInUpdate() {
        $contactRepository = new ContactRepository;
        $contactRepository->save(['email' => 'heric@gmail.com']);
        $contact = Contact::whereEmail('heric@gmail.com')->first();
        $this->assertTrue($contactRepository->save(['id' => $contact->id, 'email' => $contact->email]));
        
    }

    /**
     * @expectedException Exception
     */
    public function testUseExistEmailInUpdateError() {
        $contactRepository = new ContactRepository;
        $contactRepository->save(['email' => 'heric@gmail.com']);
        $contactRepository->save(['email' => 'heric1@gmail.com']);
        $contact = Contact::whereEmail('heric@gmail.com')->first();
        $contactRepository->save(['id' => $contact->id, 'email' => 'heric1@gmail.com']);        
    }

    public function testSaveWithAddress() {
        $contactRepository = new ContactRepository;
        $this->assertTrue($contactRepository->save([
            'email' => 'heric@gmail.com', 
            'address' => [
                'code' => '23030-380'
            ]
        ]));        
    }
}
