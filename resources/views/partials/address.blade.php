<div class="row">
    <div class="col-xs-12 col-md-3">
        <div class="form-group">
            <label for="code">Cep:</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-compass"></i></span>
                </div>
                @include('partials.input', ['nameId' => 'code', 'value' => $code, 'attr' => ['required']])
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="form-group">
            <label for="uf">Uf:</label>
            @include('partials.input', ['nameId' => 'uf', 'value' => $uf, 'attr' => ['readonly']])
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="form-group">
            <label for="city">City:</label>
            @include('partials.input', ['nameId' => 'city', 'value' => $city, 'attr' => ['readonly']])
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="form-group">
            <label for="city">District:</label>
            @include('partials.input', ['nameId' => 'district', 'value' => $district, 'attr' => ['readonly']])
        </div>
    </div>

    <div class="col-md-8">
        <div class="form-group">
            <label for="street">Street:</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-road"></i></span>
                </div>
                @include('partials.input', ['nameId' => 'street', 'value' => $street])
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="number">Number:</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-sign"></i></span>
                </div>
                @include('partials.input', ['nameId' => 'number', 'value' => $number])
            </div>
        </div>
    </div>
    @include('partials.input', ['nameId' => 'lat', 'value' => $lat, 'type' => 'hidden'])
    @include('partials.input', ['nameId' => 'lng', 'value' => $lng, 'type' => 'hidden'])
</div>

@push('scripts')
<script>
    $().ready(function(){
        $('.code').mask('00000-000').blur(function(){
            var $val = $(this).val();
            $content = $(this).parents('.row');
            $.ajax({
                url: 'https://maps.googleapis.com/maps/api/geocode/json?address='+$val+"&key={{env('GOOGLE_API_KEY')}}"
            }).done(function(data) {
                $content.find('.uf').val(data.results[0].address_components[3].short_name);
                $content.find('.city').val(data.results[0].address_components[2].short_name);
                $content.find('.district').val(data.results[0].address_components[1].short_name);
                $content.find('.lat').val(data.results[0].geometry.location.lat);
                $content.find('.lng').val(data.results[0].geometry.location.lng);                
                $content.find('.street').focus();
            });
        });        
    });
</script>
@endpush