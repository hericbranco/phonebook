<input 
name="{{$nameId}}" 
id="{{$nameId}}" 
type="{{(isset($type))? $type : '' }}" 
class="form-control {{$nameId}}" 
value="{{(old($nameId))?old($nameId):$value}}"
{{(isset($attr) && is_array($attr))?implode(' ', $attr):''}}>