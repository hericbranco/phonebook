@extends('layouts.default')
@section('title', 'Contacts')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div style="display: block;margin:0 auto;max-width:950px;" id="gchart" data-title="Cadastros por dia" data-url="{{url('contacts/ajax')}}"></div>
        </div>
        <div class="col-md-6">
            <div id="chart_div" data-title="Cadastros por dia" data-url="{{url('contacts/ajax')}}"></div>
        </div>
        <div class="col-md-6">
            <div id="map" style="width=100%;height:450px" data-url="{{url('contacts/getLatLng')}}"></div>
        </div>
    </div>

@stop

@push('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script 
src="https://maps.googleapis.com/maps/api/js?libraries=visualization&key={{env('GOOGLE_API_KEY')}}">
    </script>

    <script type="text/javascript">
        google.charts.load('current', {'packages':['calendar']});      
        google.charts.setOnLoadCallback(drawDayChart);

        function drawDayChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Date' });
            dataTable.addColumn({ type: 'number', id: 'Qtd' });                
            $.ajax({
                url: $('#gchart').attr('data-url')
            }).done(function(data) { 
                arrRows=[];                       
                $.each(JSON.parse(data), function(day, total){
                    arrData = day.split('-');
                    arrRows.push([new Date(arrData[0], arrData[1], arrData[2]), total]);
                });                    
                dataTable.addRows(arrRows);
                var chart = new google.visualization.Calendar(document.getElementById('gchart'));
                var options = {
                    title: $('#gchart').attr('data-title'),
                    
                    calendar: {
                        daysOfWeek: 'DSTQQSS',
                    }
                };
                chart.draw(dataTable, options);
            });           
        }

        google.charts.load('current', {'packages':['corechart', 'bar', 'line']});
        google.charts.setOnLoadCallback(drawLines);            

        function drawLines() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Date' });
            dataTable.addColumn({ type: 'number', id: 'Qtd' });  
            $.ajax({
                url: $('#chart_div').attr('data-url')
            }).done(function(data) { 
                arrRows=[];                       
                $.each(JSON.parse(data), function(day, total){
                    arrData = day.split('-');
                    arrRows.push([new Date(arrData[0], arrData[1], arrData[2]), total]);
                });                    
                dataTable.addRows(arrRows);
                var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                var options = {
                    title: $('#chart_div').attr('data-title'),
                    
                };
                chart.draw(dataTable, options);
                
            });
        }

    </script>
    <script>
        $().ready(function(){
            $('#map').each(function(){
                $url = $(this).attr('data-url');
                $id = $(this).attr('id');
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 5,                
                });

                $.ajax({
                url: $url
                }).done(function(data) { 
                    markers=[];
                    heatMarks=[];
                    $.each(JSON.parse(data), function(i, location){
                        latLng = {lat:Number(location.lat), lng:Number(location.lng)};
                        var contentString = '<div id="content">'+
                        '<div id="siteNotice">'+
                        '</div>'+
                        '<p style="margin:0">'+'<b>Cep:&nbsp;</b>'+location.code+'</p>'+
                        '<p style="margin:0">'+'<b>UF:&nbsp;</b>'+location.uf+'</p>'+
                        '<p style="margin:0">'+'<b>City:&nbsp;</b>'+location.city+'</p>'+
                        '<p style="margin:0">'+'<b>District:&nbsp;</b>'+location.district+'</p>'+
                        '<p style="margin:0">'+'<b>Email:&nbsp;</b>'+location.email+'</p>'+
                        '</div>';

                        var infowindow = new google.maps.InfoWindow({
                            content: contentString,
                            maxWidth: 200
                        });
                        
                        mark = new google.maps.Marker({
                                position: latLng,
                                title:location.name                           
                        });
                        mark.addListener('click', function() {                            
                            infowindow.open(map, markers[i]);                            
                        });
                        
                        map.setCenter(latLng);
                        markers.push(mark);
                        heatMarks.push(new google.maps.LatLng(Number(location.lat), Number(location.lng)))
                    });                    

                    //var markerCluster = new MarkerClusterer(map, markers,
                //{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

                    heatmap = new google.maps.visualization.HeatmapLayer({
                        data: heatMarks,
                        map: map
                    });

                    
                });
            });
        });
    </script>
@endpush
