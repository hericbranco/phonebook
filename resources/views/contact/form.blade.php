@extends('layouts.default')
@section('title', "Create Contacts")
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(isset($id))
        @section('title', "Edit Contacts")
        <form method="post" action="{{action('ContactController@update', $id)}}">
        <input name="_method" type="hidden" value="PATCH">
    @else
        <form method="post" action="{{url('contacts')}}">
    @endif

    {{csrf_field()}}
    <div class="form-group">
        <label for="name">Name:</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user-tie"></i></span>
            </div>
            @include('partials.input', ['nameId' => 'name', 'value' => $name])            
        </div>
    </div>

    <div class="form-group">
        <label for="email">Email:</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fa fa-at"></i></span>
            </div>
            @include('partials.input', ['nameId' => 'email', 'value' => $email, 'attr' => ['required']])            
        </div>    
    </div>

    <div class="form-group">
        <label for="telephone">Telephone:</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fa fa-phone"></i></span>
            </div>
            @include('partials.input', ['nameId' => 'telephone', 'value' => $telephone])
        </div>
    </div>

    
    @include('partials.address', $address[0])
    

    <div class="col-xs-12 text-right">  
        <a class="btn btn-danger" href="{{url('contacts')}}"><i class="fa fa-arrow-circle-left"></i>&nbsp;Voltar</a>
        <button class="btn btn-success" type="submit"><i class="fa fa-save"></i>&nbsp;Save</button>
    </div>

</form>
@stop