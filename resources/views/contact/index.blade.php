@extends('layouts.default')
@section('title', 'Contacts')
@section('content')
    @if (\Session::has('success'))
        <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
        </div>
    @endif

    <div class="col-md-12 text-right">
        <a href="{{url('contacts/create')}}" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;Add</a>
    </div>
    <div class="col-md-12">
        <table class="table table-striped mt-2">
            <thead>
                <tr>
                    <th class="d-none d-sm-block">Name</th>
                    <th style="max-width: 150px">Email</th>
                    <th class="d-none d-sm-block">Telephone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contacts as $contact)
                    <tr>
                        <td class="d-none d-sm-block">{{$contact->name}}</td>
                        <td style="max-width: 200px;overflow:hidden;">{{$contact->email}}</td>
                        <td class="d-none d-sm-block">{{$contact->telephone_masked}}</td>
                        <td>
                            <a style="display:inline-block" class="btn btn-light" href="{{action('ContactController@edit', $contact['id'])}}"><i class="fa fa-edit"></i></a>
                            <form style="display:inline-block" action="{{action('ContactController@destroy', $contact['id'])}}" method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                            </form>                
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop

